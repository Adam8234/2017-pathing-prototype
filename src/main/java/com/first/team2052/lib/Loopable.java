package com.first.team2052.lib;

@FunctionalInterface
public interface Loopable {
    void update();
}
